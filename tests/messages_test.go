package tests

import (
	"testing"
	"github.com/davecgh/go-spew/spew"
	xsellco "go-xsellco"
)


func TestCreateMessage(t *testing.T) {
	err, result := xsellco.CreateMessage([]byte(`{"thread_id":"114454508", "subject":"Sample subject line", "body":"Sample body line"}`))

	if err != nil {
		t.Errorf("Got error: %+v", err)
	}
	spew.Dump(result)
}

func TestGetMessage(t *testing.T) {
	err, result := xsellco.GetMessageById("518772633", []byte(``))

	if err != nil {
		t.Errorf("Got error: %+v", err)
	}
	spew.Dump(result)
}

func TestUpdateMessage(t *testing.T) {
	err, result := xsellco.UpdateMessageById("518772633", []byte(`{"type":"Negative Feedback"}`))

	if err != nil {
		t.Errorf("Got error: %+v", err)
	}
	spew.Dump(result)
}


func TestListMessages(t *testing.T) {
	err, result := xsellco.ListMessages("114454508", 2)

	if err != nil {
		t.Errorf("Got error: %+v", err)
	}
	spew.Dump(result)
}
