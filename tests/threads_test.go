package tests

import (
	"testing"
	"github.com/davecgh/go-spew/spew"
	xsellco "go-xsellco"
)

func TestCreateThread(t *testing.T) {
	err, result := xsellco.CreateThread([]byte(`{"channel_name":"xsellco@gmx.com", "recipient_email":"morfij@gmail.com", "recipient_name":"Stan"}`))

	if err != nil {
		t.Errorf("Got error: %+v", err)
	}
	spew.Dump(result)
}

func TestGetThread(t *testing.T) {
	err, result := xsellco.GetThreadById("114454508", []byte(``))

	if err != nil {
		t.Errorf("Got error: %+v", err)
	}
	spew.Dump(result)
}


func TestUpdateThread(t *testing.T) {
	err, result := xsellco.UpdateThreadById("114454508", []byte(`{"type":"Negative Feedback"}`))

	if err != nil {
		t.Errorf("Got error: %+v", err)
	}
	spew.Dump(result)
}


func TestThreadsGetCustomFields(t *testing.T) {
	err, result := xsellco.GetThreadsCustomFields("114454508")

	if err != nil {
		t.Errorf("Got error: %+v", err)
	}
	spew.Dump(result)
}

func TestThreadsUpdateCustomFields(t *testing.T) {
	err, result := xsellco.UpdateThreadsCustomFields("114454508", []byte(`{"my-custom-field":"new text"}`))

	if err != nil {
		t.Errorf("Got error: %+v", err)
	}
	spew.Dump(result)
}
