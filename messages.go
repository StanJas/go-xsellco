package go_xsellco

import (
	"fmt"
	"go-xsellco/models"
)

//Data Parameters
//Parameter 	Type 	Description
//thread_id 	integer 	Id of the thread the message is to be bound to REQUIRED
//direction 	string 	Permitted values: 'Incoming’, 'Outgoing’ [Defaults to 'Outgoing’]. OPTIONAL
//type 	string 	Permitted values: 'Consumer Conversation’, 'Internal Note’, 'Status Change’, 'Auto Reply’, 'Negative Feedback’, 'Negative Feedback Response’, 'Negative Feedback Resolved’, 'Resolution Case Reply’ OPTIONAL
//username 	string 	Username of the user posting the message (Required if the message has a direction 'Outgoing’)
//subject 	string 	Subject line of the message (Required if the message is not of type 'Internal Note’)
//body 	string 	REQUIRED
//created_at 	timestamp 	Types to current time OPTIONAL
//attachments 	array 	Array containing one or more attachments. Each attachment has two required fields; a name and a data field. The data field may be the URL location of an attachment or base64-encoded data. Maximum cumulative attachment size: 15 MB. OPTIONAL

func CreateMessage(params []byte) (error, *models.MessagesCreateResponse) {
	data := new(models.MessagesCreateResponse)
	endpoint := fmt.Sprintf("/messages")
	err := Post(endpoint, params, data)
	if err != nil {
		fmt.Println(err)
		return err, data
	}

	return nil, data
}

//Data Parameters
//Parameter 	Type 	Description
//id 	integer 	(Required if a thread_id is not specified)
//thread_id 	integer 	(Required if an id is not specified)
//created_after 	timestamp 	OPTIONAL
//created_before 	timestamp 	OPTIONAL
//direction 	string 	Permitted values: 'Incoming’, 'Outgoing’ OPTIONAL
//type 	string 	Permitted values: 'Consumer Conversation’, 'Internal Note’, 'Status Change’, 'Auto Reply’, 'Negative Feedback’, 'Negative Feedback Response’, 'Negative Feedback Resolved’, 'Resolution Case Reply’ OPTIONAL
//delivery_status 	string 	Permitted values: 'delivered’, 'pending delivery’, 'error delivering’, 'canceled’ OPTIONAL
//status 	string 	Permitted values: 'Open Issue’, 'Closed Issue’, 'Needs Action’, 'Scheduled’, 'Spam’ OPTIONAL'

func GetMessageById(id string, params []byte) (error, *models.MessagesGetResponse) {
	data := new(models.MessagesGetResponse)
	endpoint := fmt.Sprintf("/messages/%s", id)
	err := Get(endpoint, data)
	if err != nil {
		return err, data
	}

	return nil, data
}

//Parameter 	Type 	Description
//id 	integer 	REQUIRED
//created_at 	timestamp 	OPTIONAL
//type 	string 	Permitted values: 'Consumer Conversation’, 'Internal Note’, 'Status Change’, 'Auto Reply’, 'Negative Feedback’, 'Negative Feedback Response’, 'Negative Feedback Resolved’, 'Resolution Case Reply’ OPTIONAL

func UpdateMessageById(id string, params []byte) (error, *models.MessagesUpdateResponse) {
	data := new(models.MessagesUpdateResponse)
	endpoint := fmt.Sprintf("/messages/%s", id)
	err := Put(endpoint, params, data)
	if err != nil {
		return err, data
	}

	return nil, data
}

//URL Parameters
//Parameter 	Type 	Description
//thread_id 	integer 	Our representation of a thread ID REQUIRED
//created_after 	timestamp 	OPTIONAL
//created_before 	timestamp 	OPTIONAL
//direction 	string 	Permissible values: 'Incoming’ and 'Outgoing’ OPTIONAL
//type 	string 	Permissible values: 'Consumer Conversation’, 'Internal Note’, 'Status Change’, 'Auto Reply’, 'Negative Feedback’, 'Negative Feedback Response’, 'Negative Feedback Resolved’ OPTIONAL
//status 	string 	Permissible values: 'Open Issue’, 'Closed Issue’, 'Needs Action’, 'Spam’ OPTIONAL'

func ListMessages(threadID string, limit int) (error, *models.MessagesListResponse) {
	data := new(models.MessagesListResponse)
	endpoint := fmt.Sprintf("/messages?thread_id=%s&limit=%d", threadID, limit)
	err := Get(endpoint, data)
	if err != nil {
		return err, data
	}

	return nil, data
}
