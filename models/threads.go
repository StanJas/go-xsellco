package models

type ThreadsCreateResponse struct {
	Meta []string `json:"meta,omitempty"`
	Code int `json:"code"`
	Object string `json:"object"`
	Data struct {
		ID string `json:"id"`
		OrderID string `json:"order_id"`
		SellerOrderID string `json:"seller_order_id"`
		Type string `json:"type"`
		Status string `json:"status"`
		MostRecentlyUpdated string `json:"most_recently_updated,omitempty"`
		CreatedAt string `json:"created_at"`
		FromEmail string `json:"from_email"`
		FromPhone string `json:"from_phone_number,omitempty"`
		FromName string `json:"from_name"`
		AssignedUsers []string `json:"assigned_users,omitempty"`
		MessageIDs string `json:"message_ids,omitempty"`
	} `json:"data"`
	Time int `json:"time"`
}

type ThreadsListResponse struct {
	Meta []string `json:"meta,omitempty"`
	Code int `json:"code"`
	Object string `json:"object"`
	Page int `json:"page"`
	PageItems int `json:"page_items"`
	HasMore bool `json:"has_more"`
	Data []struct {
		ID string `json:"id"`
		OrderID string `json:"order_id"`
		SellerOrderID string `json:"seller_order_id"`
		Type string `json:"type"`
		Status string `json:"status"`
		MostRecentlyUpdated string `json:"most_recently_updated,omitempty"`
		CreatedAt string `json:"created_at"`
		FromEmail string `json:"from_email"`
		FromPhone string `json:"from_phone_number,omitempty"`
		FromName string `json:"from_name"`
		AssignedUsers []string `json:"assigned_users,omitempty"`
		MessageIDs string `json:"message_ids,omitempty"`
	} `json:"data"`
	Time int `json:"time"`
}

type ThreadsGetCustomFieldsResponse struct {
	Meta []string `json:"meta,omitempty"`
	Code int `json:"code"`
	Object string `json:"object"`
	Page int `json:"page"`
	PageItems int `json:"page_items"`
	HasMore bool `json:"has_more"`
	Data []struct {
		ID string `json:"id"`
		Type string `json:"type"`
		Name string `json:"name"`
		Value string `json:"value"`
	} `json:"data"`
	Time int `json:"time"`
}

type ThreadsUpdateCustomFieldsResponse struct {
	Meta []string `json:"meta,omitempty"`
	Code int `json:"code"`
	Object string `json:"object"`
	Data []string `json:"data"`
	Time int `json:"time"`
}
