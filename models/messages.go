package models

//import "github.com/shopspring/decimal"

type MessagesCreateResponse struct {
	Meta []string `json:"meta"`
	Code int `json:"code"`
	Object string `json:"object"`
	Data struct {
		ID string `json:"id"`
		ThreadID int `json:"thread_id"`
		CreatedAt struct {
			Date string `json:"date"`
			TimezoneType int `json:"timezone_type"`
			Timezone string `json:"timezone"`
		} `json:"created_at"`
		Subject string `json:"subject"`
		Body string `json:"body"`
		Direction string `json:"direction"`
		Delivered bool `json:"delivered"`
		RespondBy string `json:"respond_by"`
		Type string `json:"type"`
	} `json:"data"`
	Time int `json:"time"`
}


type MessagesGetResponse struct {
	Meta []string `json:"meta"`
	Code int `json:"code"`
	Object string `json:"object"`
	Data []struct {
		ID string `json:"id"`
		ThreadID string `json:"thread_id"`
		CreatedAt string `json:"created_at"`
		Subject string `json:"subject"`
		Body string `json:"body"`
		Direction string `json:"direction"`
		Delivered bool `json:"delivered"`
		RespondBy string `json:"respond_by"`
		Type string `json:"type"`
	} `json:"data"`
	Time int `json:"time"`
}


type MessagesUpdateResponse struct {
	Code int `json:"code"`
	Data struct {
		ID string `json:"id"`
		ThreadID string `json:"thread_id"`
		CreatedAt string `json:"created_at"`
		Subject string `json:"subject"`
		Body string `json:"body"`
		Direction string `json:"direction"`
		Delivered bool `json:"delivered"`
		RespondBy string `json:"respond_by"`
		Type string `json:"type"`
	} `json:"data"`
	Time int `json:"time"`
}


type MessagesListResponse struct {
	Meta []string `json:"meta"`
	Code int `json:"code"`
	Object string `json:"object"`
	Page int `json:"page"`
	PageItems int `json:"page_items"`
	HasMore bool `json:"has_more"`
	Data []struct {
		ID string `json:"id"`
		ThreadID string `json:"thread_id"`
		CreatedAt string `json:"created_at"`
		Subject string `json:"subject"`
		Body string `json:"body"`
		Direction string `json:"direction"`
		Delivered bool `json:"delivered"`
		RespondBy string `json:"respond_by"`
		Type string `json:"type"`
	} `json:"data"`
	Time int `json:"time"`
}
