package go_xsellco

import (
	"fmt"
	"go-xsellco/models"
)

//Parameter 	Type 	Description
//channel_name 	string 	REQUIRED
//status 	string 	Permitted values: 'Open Issue’, 'Closed Issue’, 'Needs Action’, 'Scheduled’, 'Spam’. (Types to 'Open Issue’) OPTIONAL
//seller_order_id 	string 	Seller’s representation of an order ID; for example, long-string Amazon order id OPTIONAL
//created_at 	timestamp 	OPTIONAL
//username 	string 	Username of the support agent assigned to the thread OPTIONAL
//recipient_email 	string 	Consumer’s e-mail address (Required if a seller_order_id is not specified)
//recipient_name 	string 	(Required if a seller_order_id is not specified)
//recipient_phone_number 	string 	OPTIONAL
//usernames 	array of strings 	Array of usernames of those users assigned to the thread OPTIONAL
//type 	string 	Permitted values: 'Returns’, 'Cancellation’, 'Wrong Item’, 'Defective Item’, 'Shipping Query’, 'Product Query’, 'Order Query’, 'Resolution Case’, 'Other Query’, 'Negative Feedback’, 'System Message’, 'Contact Buyer’, 'Payments Query’, 'High5 Reply’ OPTIONAL

func CreateThread(params []byte) (error, *models.ThreadsCreateResponse) {
	data := new(models.ThreadsCreateResponse)
	endpoint := fmt.Sprintf("/threads")
	err := Post(endpoint, params, data)
	if err != nil {
		fmt.Println(err)
		return err, data
	}

	return nil, data
}

//Parameter 	Type 	Description
//id 	string 	REQUIRED
//channel_name 	string 	(Required if a valid id is not specified)
//status 	string 	Permitted values: 'Open Issue’, 'Closed Issue’, 'Needs Action’, 'Scheduled’, 'Spam’. (Types to 'Open Issue’) OPTIONAL
//seller_order_id 	string 	Seller’s representation of an order ID; for example, long-string Amazon order id OPTIONAL
//created_after 	timestamp 	OPTIONAL
//created_before 	timestamp 	OPTIONAL
//username 	string 	Username of the support agent assigned to the thread OPTIONAL
//recipient_email 	string 	E-mail of the consumer OPTIONAL

func GetThreadById(id string, params []byte) (error, *models.ThreadsCreateResponse) {
	data := new(models.ThreadsCreateResponse)
	endpoint := fmt.Sprintf("/threads/%s", id)
	err := Get(endpoint, data)
	if err != nil {
		return err, data
	}

	return nil, data
}

//Parameter 	Type 	Description
//id 	integer 	Required
//status 	string 	Permitted values: 'Open Issue’, 'Closed Issue’, 'Need Action’, 'Scheduled’, 'Spam’ OPTIONAL
//type 	string 	Permitted values: 'Returns’, 'Cancellation’, 'Wrong Item’, 'Defective Item’, 'Shipping Query’, 'Product Query’, 'Order Query’, 'Resolution Case’, 'Other Query’, 'Negative Feedback’, 'System Message’, 'Contact Buyer’, 'Payments Query’, 'High5 Reply’ OPTIONAL

func UpdateThreadById(id string, params []byte) (error, *models.ThreadsCreateResponse) {
	data := new(models.ThreadsCreateResponse)
	endpoint := fmt.Sprintf("/threads/%s", id)
	err := Put(endpoint, params, data)
	if err != nil {
		return err, data
	}

	return nil, data
}

//Parameter 	Type 	Description
//id 	integer 	Our ID representation of the thread REQUIRED
//channel_name 	string 	Name of the channel to which the thread belongs. Required in the absence of a 'type’ parameter.
//type 	string 	Permissible types: 'Returns’, 'Cancellation’, 'Wrong Item’, 'Defective Item’, 'Shipping Query’, 'Product Query’, 'Order Query’, 'Resolution Case’, 'Other Query’, 'Negative Feedback’, 'Contact Buyer’, 'Payments Query’. Case-insensitive. Required in the absence of 'channel_name’ parameter.
//seller_order_id 	string 	Your representation of the order ID to which thread(s) may belong. OPTIONAL
//status 	string 	Permissible statuses: 'Open Issue’, 'Closed Issue’, 'Needs Action’, 'Scheduled’, 'Spam’ OPTIONAL.
//created_after 	timestamp 	OPTIONAL
//created_before 	timestamp 	OPTIONAL
//recipient_email 	string 	If specified, a valid channel_name must also be specified.'''

func ListThreads(channelID string, page int, limit int) (error, *models.ThreadsListResponse) {
	data := new(models.ThreadsListResponse)
	endpoint := fmt.Sprintf("/threads?channel_name=%s&page=%d&limit=%d", channelID, page, limit)
	err := Get(endpoint, data)
	if err != nil {
		return err, data
	}

	return nil, data
}

//Parameter 	Type 	Description
//id 	integer 	Our ID representation of the thread/ticket REQUIRED

func GetThreadsCustomFields(id string) (error, *models.ThreadsGetCustomFieldsResponse) {
	data := new(models.ThreadsGetCustomFieldsResponse)
	endpoint := fmt.Sprintf("/threads/%s/custom-field", id)
	err := Get(endpoint, data)
	if err != nil {
		return err, data
	}

	return nil, data
}

//URL Parameters
//Parameter 	Type 	Description
//id 	integer 	Our ID representation of the thread/ticket REQUIRED

//Data Parameters
//Parameter 	Type 	Description
//custom-field-name 	string 	This is the name of the custom field you have created. You may set many custom field values in a single request REQUIRED

func UpdateThreadsCustomFields(id string, params []byte) (error, *models.ThreadsUpdateCustomFieldsResponse) {
	data := new(models.ThreadsUpdateCustomFieldsResponse)
	endpoint := fmt.Sprintf("/threads/%s/custom-field", id)
	err := Post(endpoint, params, data)
	if err != nil {
		return err, data
	}

	return nil, data
}
