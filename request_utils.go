package go_xsellco

import (
	"bytes"
	"fmt"
	"encoding/json"
	"errors"
	"net/http"
	"go-xsellco/limiter"
	"path/filepath"
	"io/ioutil"
)

var api_url = "https://api.xsellco.com/v1"
var user, pass = getCredentials()

func Get(endpoint string, data interface{}) error {
	limiter.RateLimiter.Wait()
	r, err := get(endpoint)

	if err != nil {
		return err
	}

	defer r.Body.Close()
	err = responseError(*r)
	json.NewDecoder(r.Body).Decode(&data)

	return err
}

func get(endpoint string) (*http.Response, error) {
	client := &http.Client{}
	url := api_url + endpoint
	req, _ := http.NewRequest("GET", url, nil)
	//req.SetBasicAuth("morfij@gmail.com", "Pass123")
	req.SetBasicAuth(user, pass)

	return client.Do(req)
}

//
func Post(endpoint string, params []byte, data interface{}) error {
	limiter.RateLimiter.Wait()
	r, err := post(endpoint, params)

	if err != nil {
		fmt.Println(err)
		return err
	}

	defer r.Body.Close()
	err = responseError(*r)
	json.NewDecoder(r.Body).Decode(&data)

	return err
}

func post(endpoint string, jsonStr []byte) (*http.Response, error) {
	client := &http.Client{}
	url := api_url + endpoint
	req, _ := http.NewRequest("POST", url, bytes.NewBuffer(jsonStr))
	//req.SetBasicAuth("morfij@gmail.com", "Pass123")
	req.SetBasicAuth(user, pass)

	return client.Do(req)
}

func Put(endpoint string, params []byte, data interface{}) error {
	limiter.RateLimiter.Wait()
	r, err := put(endpoint, params)

	if err != nil {
		fmt.Println(err)
		return err
	}

	defer r.Body.Close()
	err = responseError(*r)
	json.NewDecoder(r.Body).Decode(&data)

	return err
}

func put(endpoint string, jsonStr []byte) (*http.Response, error) {
	client := &http.Client{}
	url := api_url + endpoint
	req, _ := http.NewRequest("PUT", url, bytes.NewBuffer(jsonStr))
	//req.SetBasicAuth("morfij@gmail.com", "Pass123")
	req.SetBasicAuth(user, pass)

	return client.Do(req)
}
func Delete(endpoint string) error {
	r, err := delete(endpoint)
	if err != nil {
		return err
	}
	defer r.Body.Close()
	err = responseError(*r)

	return err
}

func delete(endpoint string) (*http.Response, error) {
	client := &http.Client{}
	url := api_url + endpoint
	req, _ := http.NewRequest("DELETE", url, nil)
	//token := GetAuthorizationHeaderToken()
	//setHeaders(req, token)

	return client.Do(req)
}

func responseError(r http.Response) error {
	switch r.StatusCode {
	case 200, 201, 204:
		return nil
	default:
		var v map[string]interface{}
		json.NewDecoder(r.Body).Decode(&v)
		errorMessage := errors.New(v["message"].(string))
		return errorMessage
	}

}

func setHeaders(req *http.Request, token string) {
	req.Header.Set("Authorization", token)
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("User-Agent", "Go-DiscogsClient/1.0")
}

func getCredentials() (string, string) {
	cred := Credentials{}
	file, _ := filepath.Abs("../configs/credentials.json")
	b, err := ioutil.ReadFile(file)
	if err != nil {
		panic(err)
	}
	json.Unmarshal(b, &cred)

	return cred.User, cred.Pass
}

type Credentials struct {
	User string `json:"user"`
	Pass string `json:"pass"`
}
